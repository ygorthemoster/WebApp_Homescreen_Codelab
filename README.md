# Google Codelab: Add Your Web App to a User's Home Screen Codelab
This is an implementation of a Google codelab tutorial, the tutorial is freely avaliable at: https://codelabs.developers.google.com/codelabs/add-to-home-screen/index.html, this is a simple tutorial that demonstrates the basics for adding a webapp to the user's homescreen , this code is the final product of said tutorial

## Pre-requisites
To test this project you'll need
- [git](https://git-scm.com/)
- A webserver as per tutorial I'll be using firebase via the [firebase-tool](https://github.com/firebase/firebase-tools)

## Installing

First you'll need to create a new firebase project, this can be via the [firebase console](https://console.firebase.google.com/), after creating it run the following on a terminal:

```
git clone https://ygorthemoster@gitlab.com/ygorthemoster/Cloud_Functions_Codelab.git
cd Cloud_Functions_Codelab/src
firebase init
firebase deploy
```

## Running
You can start the app by running the following command on a terminal:
```
firebase open hosting:site
```
Or check out my deployed version [here](https://fireworks-60a1c.firebaseapp.com/)

this project is best viewed via mobile

## License
See [LICENSE](LICENSE)

## Acknowledgments
- Original source by [Paul Lewis](https://github.com/paullewis) available at: https://github.com/paullewis/Fireworks
- Tutorial by [Google Codelabs](https://codelabs.developers.google.com/) available at: https://codelabs.developers.google.com/codelabs/add-to-home-screen/index.html
